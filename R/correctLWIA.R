#' @title correctLWIA
#' @description Reads a LWIA file into a dataframe and than rewrites a text file with a TOA5 file header.
#' @param runDateNumber The file to be read
#' @param outpute name for the converted file
#' @export
#' @return a converted File
correctLWIA<-function(data,standards,trueDeltaD,trueDeltaO18,trueDeltaO17){
  
  calSubset<-function(i){
    subInterpolated<-setnames(subset(data,select=c('vial_name','time_interpolated',paste('delta_D_raw_mean',standards[i],sep="_"),paste('delta_O18_raw_mean',standards[i],sep="_"),paste('delta_O17_raw_mean',standards[i],sep="_"))),c('vial_name','time_interpolated','delta_D_interpolated','delta_O18_interpolated','delta_O17_interpolated'))
    trueDeltas<-data.frame(delta_D_true=trueDeltaD[i],delta_O18_true=trueDeltaO18[i],delta_O17_true=trueDeltaO17[i])
    subStandard<-setDT(cbind(subInterpolated,trueDeltas))
  }
  
  standard1_subset<-calSubset(1)
  standard2_subset<-calSubset(2)
  standards_stack<-rbind(standard1_subset,standard2_subset)
  
  lmList_D<-lmList(delta_D_true~delta_D_interpolated|time_interpolated,data=standards_stack,pool=FALSE)
  lmList_O18<-lmList(delta_O18_true~delta_O18_interpolated|time_interpolated,data=standards_stack,pool=FALSE)
  lmList_O17<-lmList(delta_O17_true~delta_O17_interpolated|time_interpolated,data=standards_stack,pool=FALSE)
  
  fitD<-setnames(cbind(rownames(coef(lmList_D)),coef(lmList_D)[1],coef(lmList_D)[2]),c('time_interpolated','c_D','m_D'))
  fitO18<-setnames(cbind(rownames(coef(lmList_O18)),coef(lmList_O18)[1],coef(lmList_O18)[2]),c('time_interpolated','c_O18','m_O18'))
  fitO17<-setnames(cbind(rownames(coef(lmList_O17)),coef(lmList_O17)[1],coef(lmList_O17)[2]),c('time_interpolated','c_O17','m_O17'))
  
  allFits<-setDT(Reduce(merge,list(fitD,fitO18,fitO17)))
  
  data[,time_interpolated:=as.character(time_interpolated)]
  mergedFit<-merge(data,allFits,by='time_interpolated')

  mergedFit[,c('delta_D_corrected','delta_O18_corrected','delta_O17_corrected'):=list((delta_D_raw_mean*m_D+c_D),(delta_O18_raw_mean*m_O18+c_O18),(delta_O17_raw_mean*m_O17+c_O17))]
  
  mergedFit[,c('z_D','z_O18'):=.(abs(approx(mergedFit[vial_name==standards[3],DateTime_mean],mergedFit[vial_name==standards[3],delta_D_corrected],mergedFit[,DateTime_mean],method='linear',rule=2)$y-trueDeltaD[3])/trueDeltaD[4],abs(approx(mergedFit[vial_name==standards[3],DateTime_mean],mergedFit[vial_name==standards[3],delta_O18_corrected],mergedFit[,DateTime_mean],method='linear',rule=2)$y-trueDeltaO18[3])/trueDeltaO18[4])]  
  
  mergedFit[z_D<2,c('assessment_D'):='acceptable']
  mergedFit[z_D>2&z_D<3,c('assessment_D'):='questionable']
  mergedFit[z_D>3,c('assessment_D'):='unacceptable']
  
  mergedFit[z_O18<2,c('assessment_O18'):='acceptable']
  mergedFit[z_O18>2&z_D<3,c('assessment_O18'):='questionable']
  mergedFit[z_O18>3,c('assessment_O18'):='unacceptable']
  
  subQC<-mergedFit[vial_name==standards[3]]
  
  par(mfrow=c(3,1),omi=c(0.75,0.75,0.5,0.0),mai=c(0.1,0.1,0.1,0.1))
  
  plot(delta_D_corrected~DateTime_mean,data=subQC,cex=1.5,xaxt='n',pch=22,bg='black',ylim=c(min(c((trueDeltaD[3]-trueDeltaD[4]),(subQC$delta_D_corrected-subQC$delta_D_raw_sd)),na.rm=T),max(c((trueDeltaD[3]+trueDeltaD[4]),(subQC$delta_D_corrected+subQC$delta_D_raw_sd)),na.rm=T)))
  errbar(subQC$DateTime_mean,subQC$delta_D_corrected,subQC$delta_D_corrected+subQC$delta_D_raw_sd,subQC$delta_D_corrected-subQC$delta_D_raw_sd,pch=22,bg="black",add=T,lwd=0.5,cex=1.5)
  abline(h=trueDeltaD[3]);abline(h=trueDeltaD[3]+trueDeltaD[4],lty=2);abline(h=trueDeltaD[3]-trueDeltaD[4],lty=2)  
  mtext(expression(delta~D~('\u2030'~VSMOW)),side=2,line=2.5,cex=0.9)
  
  plot(delta_O18_corrected~DateTime_mean,data=subQC,cex=1.5,xaxt='n',pch=22,bg='black',ylim=c(min(c((trueDeltaO18[3]-trueDeltaO18[4]),(subQC$delta_O18_corrected-subQC$delta_O18_raw_sd)),na.rm=T),max(c((trueDeltaO18[3]+trueDeltaO18[4]),(subQC$delta_O18_corrected+subQC$delta_O18_raw_sd)),na.rm=T)))
  errbar(subQC$DateTime_mean,subQC$delta_O18_corrected,subQC$delta_O18_corrected+subQC$delta_O18_raw_sd,subQC$delta_O18_corrected-subQC$delta_O18_raw_sd,pch=22,bg="black",add=T,lwd=0.5,cex=1.5)
  abline(h=trueDeltaO18[3]);abline(h=trueDeltaO18[3]+trueDeltaO18[4],lty=2);abline(h=trueDeltaO18[3]-trueDeltaO18[4],lty=2)    
  mtext(expression(delta~phantom()^18*O~('\u2030'~VSMOW)),side=2,line=2.5,cex=0.9)
  
  plot(delta_O17_corrected~DateTime_mean,data=subQC,cex=1.5,pch=22,bg='black',ylim=c(min(c((trueDeltaO17[3]-trueDeltaO17[4]),(subQC$delta_O17_corrected-subQC$delta_O17_raw_sd)),na.rm=T),max(c((trueDeltaO17[3]+trueDeltaO17[4]),(subQC$delta_O17_corrected+subQC$delta_O17_raw_sd)),na.rm=T)))
  errbar(subQC$DateTime_mean,subQC$delta_O17_corrected,subQC$delta_O17_corrected+subQC$delta_O17_raw_sd,subQC$delta_O17_corrected-subQC$delta_O17_raw_sd,pch=22,bg="black",add=T,lwd=0.5,cex=1.5)
  abline(h=trueDeltaO17[3]);abline(h=trueDeltaO17[3]+trueDeltaO17[4],lty=2);abline(h=trueDeltaO17[3]-trueDeltaO17[4],lty=2)    
  mtext(expression(delta~phantom()^17*O~('\u2030'~VSMOW)),side=2,line=2.5,cex=0.9)
  
  mtext(standards[3],side=3,line=1,outer=TRUE,cex=1.5) 
  mtext(expression(Time),side=1,line=3,outer=TRUE) 
  
  return(mergedFit)
}